<?php

namespace usermodule\user\Repositories;

use Illuminate\Database\QueryException;
use usermodule\user\Models\User;
use custommodule\custom\Exceptions\CustomException;
/**
 * Class UserRepository
 *
 */
class UserRepository
{
    public function getallrecords($client_id)
    {
    	try {
    		$IsDelete = 0;
            $userlist = User::where('IsDelete', '=', $IsDelete)
            ->where('ClientId','=',$client_id)
            ->get();
            return $userlist;
        } catch (\Exception $e) {
            throw new CustomException('Exception thrown while trying to fetch Userlist', 50001001);
        }
    }
    public function getperticularrecords($id)
    {
        try {
            $IsDelete = 0;
            $modulelist = User::where('IsDelete', '=', $IsDelete)
            ->where('Id', '=', $id)
            ->first();
            return $modulelist;
        } catch (\Exception $e) {
            throw new CustomException('Exception thrown while trying to fetch Userlist', 50001001);
        }
    }
    public function createuser($payload)
    {
        try {
            $userid = rand(0000,9999);
            // Create Dynamic module
            $dataArray=array();
            $dataArray=[
                'UserId'        => $userid,
                'FirstName'     => $payload['FirstName'],
                'LastName'      => $payload['LastName'],
                'AddressId'     => $payload['AddressId'],
                'PhoneNumber'   => $payload['PhoneNumber'],
                'Email'         => $payload['Email'],
                'Gender'        => $payload['Gender'],
                'DateofBirth'   => date('Y-m-d',strtotime($payload['DateofBirth'])),
                'Password'      => md5($payload['Password']),
                'UserTypeId'    => $payload['UserTypeId'],
                'UserImage'     => $payload['UserImage'],
                'Designation'   => $payload['Designation'],
                'RoleId'        => $payload['RoleId'],
                'ClientId'      => $payload['ClientId']
            ];
                
           $usermodule = User::create($dataArray);
           return $usermodule;

        } catch (QueryException $e) {
            
            throw new CustomException('Exception thrown while trying to create user by query', 50001001);
        } catch (\Exception $e) { 
            throw new CustomException($e->getMessage(), 50001001);
        } 
    }
    public function updateusermodule($payload,$user_id, $request,$type='all')
    {
        try {
            $userlist = $this->getperticularrecords($user_id);

            if($type=="all")
            {
                $userlist->FirstName    = $payload['FirstName'];
                $userlist->LastName     = $payload['LastName'];
                $userlist->AddressId    = $payload['AddressId'];
                $userlist->PhoneNumber  = $payload['PhoneNumber'];
                $userlist->Gender       = $payload['Gender'];
                $userlist->DateofBirth  = $payload['DateofBirth'];
                $userlist->UserImage    = $payload['UserImage'];
                $userlist->Designation  = $payload['Designation'];
                $userlist->RoleId       = $payload['RoleId'];
            }
            if($type=="active")
            {
               $userlist->IsActive = $payload['IsActive'];
            }
            if($type=="delete")
            {
               $userlist->IsDelete = $payload['IsDelete'];
            }
            
            $userlist->save();
         
            return $userlist;
        } catch (Exception $e) {
             throw new CustomException('Exception thrown while trying to update users', 50001001);
        } 
    }
    public function Destroyuser($id)
    {
       $user_module = $this->getperticularrecords($id);

        if (empty($user_module)) {
            throw new CustomException('User Module not found', 40400000);
        } else {
            $user_module->delete();
        }

        return $user_module;
    }
}