<?php
namespace usermodule\user\Models;

use Illuminate\Database\Eloquent\Model;
use usermodule\user\Models\User;

class User extends Model
{
    protected $table = 'Users';
    protected $primaryKey = 'Id';
    
    protected $fillable = ['Id','UserId','FirstName','LastName','AddressId',
    	'PhoneNumber','Email','Gender','DateofBirth','Password','UserTypeId',
    	'UserImage','IsActive','IsDelete','CreatedId','ModifiedId','Designation',
        'RoleId','ClientId'];

    protected $visible = ['Id','UserId','FirstName','LastName', 'AddressId',
    	'PhoneNumber','Email','Gender','DateofBirth','Password','UserTypeId',
    	'UserImage','IsActive','IsDelete','CreatedId','ModifiedId','Designation',
        'RoleId','ClientId'];

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
}